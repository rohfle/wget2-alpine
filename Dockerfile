FROM alpine:latest

RUN apk add --no-cache wget2

WORKDIR /downloads

ENTRYPOINT [ "wget2" ]